<?php

session_start();
require 'vendor/connect.php';

?>

<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <style type="text/css">
  	.error{

  		background-color: salmon;
  	}

  </style>

  <title>Product adding</title>
</head>

<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">

    function showBlock(val){
        document.getElementById('id1').style.display='none';
        document.getElementById('id2').style.display='none';
        document.getElementById('id3').style.display='none'; 
        document.getElementById('id'+val).style.display='block';        
    }

   function checksymb(inputId) {
    	$(inputId).focus(function() {
    		$(inputId).removeClass('error').next().empty();
    	});

    	$(inputId).blur(function() {
    		var data = $(inputId).val();
        var name = $(inputId).attr('name');
       
    			if (data != '') {
          			$.ajax({

            			url: 'vendor/checksymb.php',
            			type: 'POST',
            			data: {data: data, check: name},
            			success: function(k) {
            				
            				if (k == 'yes') {
            					$(inputId).removeClass('error');
            					$(inputId).next().text();
            				}else if(k == 'no') {
            					$(inputId).addClass('error');
            					$(inputId).next().text('The gap has to contain only numbers');
            				}
          
            			},
            			error: function() {
              				$(inputId).next().text('error');
            			}
          			});

        		}else{
        			$(inputId).addClass('error');
          			$(inputId).next().text('Fill in the gap');
        		}
    	});
    } 
$(document).ready(function(){
  checksymb("#price");
  checksymb("#size");
  checksymb("#weight");
  checksymb("#height");
  checksymb("#width");
  checksymb("#length");

});

  $(document).ready(function() {

    $("#sku").focus(function() {
        $("#sku").removeClass('error').next().empty();
      });
      $("#sku").blur(function() {
        var data = $("#sku").val();
        if (data != '') {
          $.ajax({

            url: 'vendor/checksku.php',
            type: 'POST',
            data: {sku: data},
            success: function(res){

              if (res == 'yes') {
                
                $("#sku").next().text();
                $("#sku").removeClass('error');
              } else if(res == 'no') {
                
                $("#sku").next().text('Given SKU already exist');
                $("#sku").addClass('error');
                
              }

          
            },
            error: function() {
              $("#sku").next().text('error');
              $("#sku").addClass('error');
            }
          });

        }else{

          $("#sku").next().text('Fill in the gap');
          $("#sku").addClass('error');

        }
      });
});

  $(document).ready(function() {

    $("#sku").focus(function() {
        $("#sku").removeClass('error').next().empty();
      });
      $("#sku").blur(function() {
        var data = $("#sku").val();
        if (data != '') {
          $.ajax({

            url: 'vendor/checksku2.php',
            type: 'POST',
            data: {sku: data},
            success: function(x){

              if (x == 'yes') {
                
                $("#sku").next().text();
                $("#sku").removeClass('error');
              } else if(x == 'no') {
                $("#sku").addClass('error');
                $("#sku").next().text('SKU has to contain A-Z and 0-9');

              }

          
            },
            error: function() {
              $("#sku").next().text('error');
              $("#sku").addClass('error');
            }
          });

        }else{

          $("#sku").next().text('Fill in the gap');
          $("#sku").addClass('error');

        }
      });
});

    $(document).ready(function() {

    $("#name").focus(function() {
        $("#name").removeClass('error').next().empty();
      });
      $("#name").blur(function() {
        var data = $("#name").val();
        if (data != '') {
          $.ajax({

            url: 'vendor/checkname.php',
            type: 'POST',
            data: {name: data},
            success: function(g){

              if (g == 'yes') {
                
                $("#name").next().text();
                $("#name").removeClass('error');
              } else if(g == 'no') {
                
                $("#name").next().text('Name has to contain A-z and 0-9');
                $("#name").addClass('error');
                
              }

          
            },
            error: function() {
              $("#name").next().text('error');
              $("#name").addClass('error');
            }
          });

        }else{

          $("#name").next().text('Fill in the gap');
          $("#name").addClass('error');

        }
      });
});

  
</script>


  <div class="wrapper">
    <header>

      <a href="index.php"><button type="button" class="btn btn-primary">Cancel</button></a>
      <a href="index.php"><input type="submit" class="btn btn-primary" form="form1" value="Save"></a>
      <div class="header"><h2>Product adding</h2></div>

    </header>

    <content>
      <form id="form1" action="vendor/add.php" method="POST" name="form1">

          <label>SKU</label>
              <input type="text" class="labels1" name="sku" id="sku" placeholder="Enter SKU" minlength="3" maxlength="8"> <b></b><br><br>
              
              
            <label>Name</label>
              <input type="text" class="labels2" name="name" id="name" required="" placeholder="Enter name" minlength="2" maxlength="20"> <b></b><br><br>
 
                
            <label>Price</label>
              <input type="text" class="labels3" name="price" id="price" required="" placeholder="Enter Price"> <b></b><br><br>

            <label>Type switcher</label>
              <select name="type" onchange="showBlock(this.value)" id="type" required="" class="labels4">

                <option value="" selected="selected"></option>
            <option name="DVD-Disc" value="1">DVD-Disc</option>
            <option name="Book" value="2">Book</option>
           <option name="Furniture" value="3">Furniture</option>

          </select>
          <br>


           <div id="id1" class="blocks">
            <label>Size (MB)</label>
            <input type="text" name="size" id="size"   placeholder="Please provide size"> <b></b>
          </div>
          

          <div id="id2" class="blocks">
            <label>Weight (KG)</label>
            <input type="text" name="weight" id="weight"  placeholder="Please provide weight"> <b></b>
          </div>

          
          <div id="id3" class="blocks">
            <label>Height (CM)</label>
            <input type="text" name="height" id="height"  placeholder="Please provide height"> <b></b>
            <br><br>

            <label>Width (CM)</label>
            <input type="text" name="width" id="width"  placeholder="Please provide width"> <b></b>
            <br><br>

            <label>Length (CM)</label>
            <input type="text" name="length" id="length"  placeholder="Please provide length"> <b></b>
            
          </div>
        </form>

    </content>


    <footer>
      <div class="addfooter"><h5> Scandiweb test assignment </h5></div>
    </footer>

  </div>
</body>

</html>