<?php

session_start();
require 'vendor/connect.php';

?>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<title>Product deleting</title>
</head>
<body>
   
    
	<div class="wrapper">
		<header>

			<a href="index.php"><button type="button" class="btn btn-primary">Cancel</button></a>
			<input type="submit" class="btn btn-primary" id="but" form="deleteform" name="del"value="Save">
			<div class="header"><h2>Product deleting</h2></div>

		</header>

			<content>
				<ul>
					<form method="POST" action="" id="deleteform">

<?php 

	
 	  	$sql = mysqli_query($connect, "SELECT `id`,`sku`, `name`, `price`, `type`, `size`, `weight`,`height`, `width`, `length` FROM `products2");
 		while ($result = mysqli_fetch_array($sql)) {
 		    for($i=0; $i<count(array($result['id'])); $i++){
 				$id = $result['id'];
 				$sku = $result['sku'];
 				$name = $result['name'];
 				$price = $result['price'];
 				$type = $result['type'];
 				$size = $result['size'];
 				$weight = $result['weight'];
 				$height = $result['height'];
 				$width = $result['width'];
 				$length = $result['length'];
?>


 <?php


 			if ($result['type'] === 'DVD-Disc') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 				<input type='checkbox' name='check[]' id='check[]' value='{$id}'>
 				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Size: {$result['size']} MB</div>
 			</div>
 			</li>
 			"	;
 		}

 		if ($result['type'] === 'Book') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 				<input type='checkbox' name='check[]' id='check[]' value='{$id}'>
 				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Weight: {$result['weight']} KG</div>
 			</div>
 			</li>";

 		
 		}

 		if ($result['type'] === 'Furniture') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 			<input type='checkbox' name='check[]' id='check[]' value='{$id}'>
				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Dimension: {$result['height']}x{$result['width']}x{$result['length']}</div>
 			</div>
 			</li>";

 		
 		}
}
}


if (empty($_POST['check'])) {
	echo "<script> function foo(){ but.disabled = !check[].checked; } foo(); check[].addEventListener('change'.foo); alert('choose minimum one product'); </script";
}elseif(!empty($_POST['check'])){
		$check=$_POST['check'];
			foreach ($check as $id) {

			$res4=mysqli_query($connect, "DELETE FROM `products2` WHERE `id` = '$id'");

			echo "<script>document.location.replace('index.php');</script>";

		}
		}
?>

		</form>
 	</ul>


			</content>


		<footer>
			<div class="footer"><h5> Scandiweb test assignment </h5></div>
		</footer>
	</div>

</body>
</html>