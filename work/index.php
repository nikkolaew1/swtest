<?php

session_start();
require 'vendor/connect.php';

?>

<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<title>Product list</title>
</head>
<body>
   
    
	<div class="wrapper">
		<header>

			<a href="deleting.php"><button type="button" class="btn btn-primary">Mass Delete</button></a>
			<a href="adding.php"><button type="button" class="btn btn-primary">Add</button></a>
			<div class="header"><h2>Product list</h2></div>

		</header>

			<content>
				<ul>
<?php 

 	  	$sql = mysqli_query($connect, "SELECT `sku`, `name`, `price`, `type`, `size`, `weight`,`height`, `width`, `length` FROM `products2");
 		while ($result = mysqli_fetch_array($sql)) {

 			if ($result['type'] === 'DVD-Disc') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 				<input type='checkbox' class='box'>
 				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Size: {$result['size']} MB</div>
 			</div>
 			</li>
 			"	;
 		}

 		if ($result['type'] === 'Book') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 				<input type='checkbox' class='box'>
 				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Weight: {$result['weight']} KG</div>
 			</div>
 			</li>";

 		
 		}

 		if ($result['type'] === 'Furniture') {
 			
 			  echo "
 			<li>
 			<div class='product'>
 			<input type='checkbox' class='box'>
				<div class='skus'>{$result['sku']}</div>
 				<div class='name'>{$result['name']}</div>
 				<div class='price'>{$result['price']}$</div>
 				<div class='desc'>Dimension: {$result['height']}x{$result['width']}x{$result['length']}</div>
 			</div>
 			</li>";

 		
 		}

 	} 



?>
 	</ul>


			</content>


		<footer>
			<div class="footer"><h5> Scandiweb test assignment </h5></div>
		</footer>
	</div>

</body>
</html>